document.addEventListener('DOMContentLoaded', function () {
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, {
            action: "requestSettings",
        }, function (response) {
            let checkbox = document.getElementById('enabledCheckbox');
            let slider = document.getElementById('volumeSlider');
            let input = document.getElementById('current_volume');

            let enabled = true;
            let volume = 0.03;

            if (response != null) {
                enabled = response.enabled;
                volume = response.volume;
            }

            checkbox.checked = enabled;
            slider.value = volume * 100;
            input.value = slider.value;


        });
    });
    document.getElementById('volumeSlider').addEventListener('input', changeVolume);
    document.getElementById('enabledCheckbox').addEventListener('change', changeEnabled);
    document.getElementById('current_volume').addEventListener('input', changeVolume);
});

changeVolume = function () {
    let slider = document.getElementById('volumeSlider');
    let input = document.getElementById('current_volume');
    if (this.type == "range") {
        input.value = slider.value;
    } else if (this.type == "number" || this.type == "text") {
        slider.value = input.value;
    }

    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, {
            action: "changeVolume",
            volume: slider.value / 100
        });
    });
}
changeEnabled = function () {
    let input = document.getElementById('enabledCheckbox');

    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, {
            action: "changeEnabled",
            enabled: input.checked
        });
    });
}