const url = window.location.hostname;

function getSettings() {
    let settings;
    try {
        console.log('trying JSON.parse');
        settings = JSON.parse(localStorage.getItem(url));
    } catch {
        console.log('JSON was invalid');
        settings = null;
    }
    if (settings == null) {
        console.log('loading default options');
        settings = {
            enabled: true,
            volume: 0.03,
        }
    }
    console.log(settings);
    console.log('settings loaded');
    return settings;
}
function setSettings() {
    localStorage.setItem(url, JSON.stringify(window.settings));
    console.log(window.settings);
    console.log('settings saved');
}

function initListeners() {
    chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
        switch (request.action) {
            case 'changeVolume':
                if (window.settings.volume != request.volume) {
                    window.settings.volume = request.volume;
                    console.log('changed volume');
                    setSettings();
                }
                break;
            case 'changeEnabled':
                window.settings.enabled = request.enabled;
                if (window.settings.enabled) {
                    console.log('AER has been enabled');
                    initInterval();
                } else {
                    console.log('AER has been disabled');
                    clearInterval(window.interval);
                    console.log('cleared interval ' + window.interval);
                }
                setSettings();
                break;
            case 'requestSettings':
                sendResponse(window.settings);
                break;
        }
    });
    console.log('initialized listeners');
}

function initInterval() {
    window.interval = setInterval(function () {
        for (let i = 0; i < window.videos.length; i++) {
            window.videos[i].volume = window.settings.volume;
        }
        for (let i = 0; i < window.audios.length; i++) {
            window.audios[i].volume = window.settings.volume;
        }
        if (false) {
            let info = {
                videosCount: window.videos.length,
                audiosCount: window.audios.length,
                settings: window.settings,
            }
            console.log(info);
        }
    }, 150);

    console.log('initialized interval ' + window.interval);
}

function main() {
    initListeners();

    window.videos = document.getElementsByTagName("video");
    window.audios = document.getElementsByTagName("audio");

    window.settings = getSettings();

    if (window.settings.enabled == false) {
        console.log('AER is disabled');
        return;
    }

    initInterval();
}

main();
