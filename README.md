# AER extension

Lowers the volume on all videos and audio, with memory

Installation:
1. download the extension
2. go to the extension page
3. enable developer mode
4. click "load upacked"
5. select the folder with the extension

TODO:
- lower volume on all volume-producing elements
- change the "setInterval" to an event
- better UI
